/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.util.List;

/**
 *
 * @author urwah
 */
public class Research {
    private double waitTime;
    private List<Patient>patientList;
    
    public Research(double waitTime, List<Patient>patientList){
        this.waitTime=waitTime;
        this.patientList=patientList;
        System.out.println("Average waiting time: "+waitTime+" hours ");
    }
      public double getWaitTime(){
          return this.waitTime;
      }      
    public List<Patient>getPatientList(){
        return this.patientList;
    }
}
