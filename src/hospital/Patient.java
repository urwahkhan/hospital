/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;
import java.util.Date;

/**
 *
 * @author urwah
 */
public class Patient {
    private String name;
    private String DoB;
    private String doctor;
    
    public Patient(String name,String DoB, String doctor){
        this.name=name;
        this.DoB=DoB;
        this.doctor=doctor;
        
        System.out.println("Patient name:"+name+"DoB: "+DoB+"family Doctor: "+doctor);
    }
    public String getName(){
        return this.name;
       
    }
    public String getDoB(){
        return this.DoB;
        
    }
    public String getDoctor(){
        return this.doctor;
    }
    
}
