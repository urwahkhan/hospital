/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author urwah
 */
public class Hospital {

    public static void main(String[] args) {
    
        Patient p1 = new Patient("Rubina ","28/1/1977 ","Dr.Khan");
        Patient p2 = new Patient("Arshina ","3/5/1877 ","Dr.Mir");
        Patient p3 = new Patient("Haseena ","11/6/1333 ","Dr.Patel");
        Patient p4 = new Patient("Sakeena ","4/12/1200 ","Dr.Khan");
        
         List<Patient> ER= new ArrayList<Patient>();
        ER.add(p1);
        ER.add(p2);
        ER.add(p3);
        ER.add(p4);
        
        WristBand[] wb= new WristBand[4];
        wb[0]= new WristBand (2433435, "Tooth ache");
        wb[1]= new WristBand (2433435, "Diabetes");
        wb[2]= new WristBand (2433435, "Cancer");
        wb[3]= new WristBand (2433435, "Fracture");
        
        Research r= new Research(11.5,ER);
        
      System.out.println(ER);
      System.out.println(wb);
      System.out.println(r);
    }
    
}
